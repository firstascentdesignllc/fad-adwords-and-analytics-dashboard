<?php
/**
 * Plugin Name: FAD AdWords and Analytics Dashboard
 * Plugin URI: http://firstascentdesign.com/
 * Description: Create and provide clean beatifull shortcode.
 * Version: 1.0
 * Author: First Ascent Design
 * Author URI: http://firstascentdesign.com/
 * Requires at least: 4.1
 * Tested up to: 4.9.7
 * License: GPL2
 *
 * Text Domain: fad-aa
 * Domain Path: /languages
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Check if class already exist
if( ! class_exists('FAD_AdWords_Analytics_Dashboard')) :

/**
 * Main WP FAD Adwords and Analytics Dashboard Search Class
 *
 * @class FAD_AdWords_Analytics_Dashboard
 * @version	1.0
 */

final class FAD_AdWords_Analytics_Dashboard {
	/**
	 * @var FAD_AdWords_Analytics_Dashboard The single instance of the class
	 * @since 2.1
	 */
	protected static $_instance = null;
	
	/**
	 * Main FAD_AdWords_Analytics_Dashboard Instance
	 *
	 * Ensures only one instance of FAD_AdWords_Analytics_Dashboard is loaded or can be loaded.
	 *
	 * @since 2.1
	 * @static
	 * @see WC()
	 * @return FAD_AdWords_Analytics_Dashboard - Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * Cloning is forbidden.
	 * @since  1.0
	 * @access public
	 * @return void
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'fad-aa' ), '1.0' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 * @since  1.0
	 * @access public
	 * @return void
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'fad-aa' ), '1.0' );
	}

	/**
	 * Magic method to prevent a fatal error when calling a method that doesn't exist.
	 *
	 * @since  1.0
	 * @access public
	 * @return void
	 */
	public function __call( $method = '', $args = array() ) {
		_doing_it_wrong( "FAD_AdWords_Analytics_Dashboard::{$method}", __( 'Method does not exist.', 'fad-aa' ), '1.0' );
		unset( $method, $args );
		
		return null;
	}
	
	/**
	 * @desc	Construct the plugin object
	 */
	public function __construct()
	{
		$this->define_constants();
		$this->includes();
		$this->init_hooks();
	}
	
	/**
	 * Define Constants
	 */
	private function define_constants() {
		$this->define( 'FAD_AA_DASHBOARD_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		$this->define( 'FAD_AA_DASHBOARD_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
		$this->define( 'FAD_AA_DASHBOARD_CSS_DIR', FAD_AA_DASHBOARD_PLUGIN_URL .'assets/css' );
		$this->define( 'FAD_AA_DASHBOARD_JS_DIR', FAD_AA_DASHBOARD_PLUGIN_URL .'assets/js' );
		$this->define( 'FAD_AA_DASHBOARD_INC_PATH', FAD_AA_DASHBOARD_PLUGIN_PATH .'includes' );
		$this->define( 'FAD_AA_DASHBOARD_LIB_PATH', FAD_AA_DASHBOARD_PLUGIN_PATH .'library' );
	}
	
	/**
	 * Define constant if not already set
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 */
	public function includes() {
		include_once( FAD_AA_DASHBOARD_INC_PATH . '/fad-adwords-analytics-dashboard-actions.php' );
		include_once( FAD_AA_DASHBOARD_INC_PATH . '/class-fad-adwords-analytics-dashboard-shortcodes.php' );
	}
	
	
	/**
	 * Hook into actions and filters
	 * @since  1.0
	 */
	private function init_hooks() {

		add_action( 'init', array( $this, 'fad_aa_dashboard_init' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'fad_aa_dashboard_enqueue_styles_scripts' ) );

	}

	public function fad_aa_dashboard_init() {
		/* Front-end */
		wp_register_style( 'fad-aa-dashboard-style', FAD_AA_DASHBOARD_CSS_DIR . '/fad-aa-dashboard-style.css', false, '1.0', 'all' );
	}
	
	public function fad_aa_dashboard_enqueue_styles_scripts() {
		// Style
		wp_enqueue_style( 'fad-aa-dashboard-style' );
	}
}

endif;

/**
 * Returns the main instance of FAD_AdWords_Analytics to prevent the need to use globals.
 *
 * @since  1.0
 * @return FAD_AdWords_Analytics
 */
function FAD_AdWords_Analytics() {
	return FAD_AdWords_Analytics_Dashboard::instance();
}

// Global for backwards compatibility.
$GLOBALS['fad_aa'] = FAD_AdWords_Analytics();