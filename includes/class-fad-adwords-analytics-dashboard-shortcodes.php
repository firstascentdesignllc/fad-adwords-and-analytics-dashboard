<?php

/**
 * FAD_Adwords_Analytics_Dashboard_Shortcodes class.
 *
 * @class 		FAD_Adwords_Analytics_Dashboard_Shortcodes
 * @version		1.0
 * @author 		First Ascent Design
 */
 
if ( ! defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly
 
// Check if class already exist
if( ! class_exists('FAD_Adwords_Analytics_Dashboard_Shortcodes')) :
	
class FAD_Adwords_Analytics_Dashboard_Shortcodes {
	
	/**
	 * Init shortcodes
	 */
	public function __construct() {
		// Define shortcodes
		$shortcodes = array(
			'fad_aa_dashboard' 	=> __CLASS__ . '::fad_aa_dashboard'
		);
		
		foreach ( $shortcodes as $shortcode => $function ) {
			add_shortcode( apply_filters( "fad_aa_dashboard{$shortcode}_shortcode_tag", $shortcode ), $function );
		}
	}
	
	public static function fad_aa_dashboard( $atts ) {
		// Code here!
	}

}

return new FAD_Adwords_Analytics_Dashboard_Shortcodes();
	
endif;
// end if checking class FAD_Adwords_Analytics_Dashboard_Shortcodes() not exist