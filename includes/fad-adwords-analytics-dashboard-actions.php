<?php

/**
 * Add new options settings for WP Image Embeds
 */
function yelp_search_admin_add_page() {
	add_options_page(
		__( 'Yelp Search Settings', 'yelp-search' ),
		__( 'Yelp Search', 'yelp-search' ),
		'manage_options',
		'yelp_search_settings',
		'yelp_search_options_page'
	);
}
add_action( 'admin_menu', 'yelp_search_admin_add_page' );


function yelp_search_options_page() {
	?>
		<form action="options.php" method="post">
			<?php settings_fields( 'yelp_search_plugin_options' ); ?>
			<?php do_settings_sections( 'yelp_search_section' ); ?>
		 
			<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
		</form> 
	<?php
}

function yelp_settings_api_init() {	
 	register_setting( 'yelp_search_plugin_options', 'yelp_search_business_query' );
	
	add_settings_section(
		'yelp_search_setting_section',
		__( 'Yelp Search Settings', 'yelp-search' ),
		false,
		'yelp_search_section'
	);
	
 	add_settings_field( 'yelp_search_business-query-id', __( 'Business Query', 'yelp-search' ), 'yelp_search_business_query_fallback_function', 'yelp_search_section', 'yelp_search_setting_section' );
} 
add_action( 'admin_init', 'yelp_settings_api_init' );


function yelp_search_business_query_fallback_function() {
	echo '
		<p>
			<input type="text" name="yelp_search_business_query" id="yelp_search_business_query" value="'. get_option( 'yelp_search_business_query' ) .'" />
		</p>
		<p class="description" id="tagline-description">'. __( 'Leave blank to use the default business query <code>orthodontists</code>', 'wp-meet-the-team' ) . '.</p>
	';
}